package ru.makletsov.minesweeper.model;

public enum CellState {
    OPEN, DEFAULT, MARKED, QUESTION_MARKED
}

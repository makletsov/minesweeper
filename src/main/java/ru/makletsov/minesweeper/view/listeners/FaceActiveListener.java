package ru.makletsov.minesweeper.view.listeners;

import ru.makletsov.minesweeper.view.Presenter;
import ru.makletsov.minesweeper.view.View;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FaceActiveListener extends MouseAdapter {
    public static MouseListener get(View view, Presenter presenter) {
        return new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (presenter.isGameInProcess()) {
                    view.showScaredFace();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (presenter.isGameInProcess()) {
                    view.showNormalFace();
                }
            }
        };
    }
}

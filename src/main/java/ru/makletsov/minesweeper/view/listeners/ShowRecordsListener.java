package ru.makletsov.minesweeper.view.listeners;

import ru.makletsov.minesweeper.view.Presenter;
import ru.makletsov.minesweeper.view.RecordsPanel;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ShowRecordsListener {
    private static final String RECORDS_PANEL_TITLE = "Records";

    public static ActionListener get(Presenter presenter) {
        return e -> JOptionPane.showMessageDialog(
            null,
            new RecordsPanel(presenter.getRecords()).getPanel(),
            RECORDS_PANEL_TITLE,
            JOptionPane.PLAIN_MESSAGE
        );
    }
}

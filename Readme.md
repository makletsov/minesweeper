# Minesweeper
    
This project represents a version of the Minesweeper video-game.

It contains Java-project that is suitable for build with Gradle. To run the program your environment should maintain Java 8 or later.

You should clone the project on your local machine, then build application through run Gradle task:

`gradle fatJar`

Then find the application run file `./mvp/build/libs/mvp-1.0.0.jar` and run it through:

`java -jar mvp-1.0.0.jar`